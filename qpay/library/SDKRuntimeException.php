<?php

namespace addons\qpay\library;

class  SDKRuntimeException extends \Exception
{
    public function errorMessage()
    {
        return $this->getMessage();
    }

}
