<?php
/**
 * Note: 订单查询接口.
 * User: ysongyang <49271743@qq.com>
 * Time: 2019/4/23 0023 22:21:59
 */
namespace addons\qpay\library;


class OrderQuery extends QPayClient
{
    function __construct()
    {
        parent::__construct();
        //设置接口链接
        $this->url = "https://qpay.qq.com/cgi-bin/pay/qpay_order_query.cgi";
        //设置curl超时时间
        $this->curl_timeout = $this->config['curl_timeout'];
    }

    /**
     * 生成接口参数xml
     */
    function createXml()
    {
        try {
            //检测必填参数
            if ($this->parameters["out_trade_no"] == null &&
                $this->parameters["transaction_id"] == null) {
                throw new SDKRuntimeException("订单查询接口中，out_trade_no、transaction_id至少填一个！" . "<br>");
            }
            $this->parameters["mch_id"] = $this->config['mch_id'];//商户号
            $this->parameters["nonce_str"] = $this->createNoncestr();//随机字符串
            $this->parameters["sign"] = $this->getSign($this->parameters);//签名
            return $this->arrayToXml($this->parameters);
        } catch (SDKRuntimeException $e) {
            die($e->errorMessage());
        }
    }

}
