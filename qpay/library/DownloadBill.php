<?php
/**
 * Note: 对账单接口.
 * User: ysongyang <49271743@qq.com>
 * Time: 2019/4/23 0023 22:23:21
 */
namespace addons\qpay\library;


class DownloadBill extends QPayClient
{


    function __construct()
    {
        parent::__construct();
        //设置接口链接
        $this->url = "https://qpay.qq.com/cgi-bin/sp_download/qpay_mch_statement_down.cgi";
        //设置curl超时时间
        $this->curl_timeout = $this->config['curl_timeout'];
    }

    /**
     * 生成接口参数xml
     */
    function createXml()
    {
        try {
            if ($this->parameters["bill_date"] == null) {
                throw new SDKRuntimeException("对账单接口中，缺少必填参数bill_date！" . "<br>");
            }
            $this->parameters["mch_id"] =  $this->config['mch_id'];//商户号
            $this->parameters["nonce_str"] = $this->createNoncestr();//随机字符串
            $this->parameters["sign"] = $this->getSign($this->parameters);//签名
            return $this->arrayToXml($this->parameters);
        } catch (SDKRuntimeException $e) {
            die($e->errorMessage());
        }
    }

    /**
     *    作用：获取结果，默认不使用证书
     */
    function getResult()
    {
        $this->postXml();
        $this->result = $this->xmlToArray($this->response);
        return $this->result;
    }

}
