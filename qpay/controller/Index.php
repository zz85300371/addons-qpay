<?php

namespace addons\qpay\controller;


use addons\qpay\library\Notify;
use addons\qpay\library\OrderQuery;
use addons\qpay\library\UnifiedOrder;
use think\addons\Controller;
use Exception;
use think\Log;

/**
 * 微信支付宝插件首页
 *
 * 此控制器仅用于开发展示说明和体验，建议自行添加一个新的控制器进行处理返回和回调事件，同时删除此控制器文件
 *
 * Class Index
 * @package addons\epay\controller
 */
class Index extends Controller
{

    protected $layout = 'default';

    protected $config = [];

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
        $this->view->assign("title", "QQ支付插件");
        return $this->fetch();
    }


    public function notify_url()
    {
        #include_once("./log_.php");
        #include_once("../lib/QPay.Class.php");

        //使用通用通知接口
        $notify = new Notify();

        //存储微信的回调
        $xml = file_get_contents("php://input");
        $notify->saveData($xml);

        //验证签名，并回应微信。
        //对后台通知交互时，如果微信收到商户的应答不是成功或超时，微信认为通知失败，
        //微信会通过一定的策略（如30分钟共8次）定期重新发起通知，
        //尽可能提高通知的成功率，但微信不保证通知最终能成功。
        if ($notify->checkSign() == FALSE) {
            $notify->setReturnParameter("return_code", "FAIL");//返回状态码
            $notify->setReturnParameter("return_msg", "签名失败");//返回信息
        } else {
            $notify->setReturnParameter("return_code", "SUCCESS");//设置返回码
        }
        $returnXml = $notify->returnXml();
        echo $returnXml;

        //==商户根据实际情况设置相应的处理流程，此处仅作举例=======

        //以log文件形式记录回调信息

        //Log::record("【接收到的notify通知】:\n" . $xml . "\n");

        if ($notify->checkSign() == TRUE) {
            if ($notify->data["trade_state"] == "FAIL") {
                //此处应该更新一下订单状态，商户自行增删操作
                Log::record("【通信出错】:\n" . $xml . "\n");
            } elseif ($notify->data["trade_state"] == "FAIL") {
                //此处应该更新一下订单状态，商户自行增删操作
                Log::record("【业务出错】:\n" . $xml . "\n");
            } else {
                //此处应该更新一下订单状态，商户自行增删操作
                Log::record("【支付成功】:\n" . $xml . "\n");
            }
            //file_put_contents('qqpay.txt', xmlToArray($xml));
            $array_data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
            //商户自行增加处理流程,
            //更新订单状态
            //PayOrder::updateOrder($array_data['out_trade_no']);
            //todo...
        }
    }


    /**
     * 体验，仅供开发测试
     */
    public function experience()
    {

        if ($this->request->isAjax()) {
            $this->order_query();
        }

        $amount = $this->request->request('amount');
        $method = $this->request->request('method');
        if (!$amount || $amount < 0) {
            $this->error("支付金额必须大于0");
        }
        //订单号
        $out_trade_no = date("YmdHis") . mt_rand(100000, 999999);

        //订单标题
        $body = 'FastAdmin测试订单';
        //使用统一支付接口
        $unifiedOrder = new UnifiedOrder();
        //设置必填参数
        $unifiedOrder->setParameter("body", $body);//商品描述
        $this->view->assign("title", $body);
        $data['body'] = $body;
        $data['total_fee'] = $amount;
        $data['out_trade_no'] = $out_trade_no;
        $data['return_url'] = url('index/user/my_game'); //跳转URL
        $unifiedOrder->setParameter("out_trade_no", $out_trade_no);//商户订单号
        $unifiedOrder->setParameter("total_fee", $amount);//总金额
        $unifiedOrder->setParameter("notify_url", get_addon_config('qpay')['notify_url']);//通知地址
        $unifiedOrder->setParameter("trade_type", "NATIVE"); //支付场景 APP、JSAPI、NATIVE
        //非必填参数，商户可根据实际情况选填
        //$unifiedOrder->setParameter("device_info","XXXX");//设备号
        //$unifiedOrder->setParameter("attach","XXXX");//附加数据
        //$unifiedOrder->setParameter("time_start","XXXX");//交易起始时间
        //$unifiedOrder->setParameter("time_expire","XXXX");//交易结束时间
        //获取统一支付接口结果
        $unifiedOrderResult = $unifiedOrder->getResult();
        //商户根据实际情况设置相应的处理流程
        if ($unifiedOrderResult["return_code"] == "FAIL") {
            //商户自行增加处理流程
            #echo "通信出错：" . $unifiedOrderResult['return_msg'] . "<br>";
        } elseif ($unifiedOrderResult["result_code"] == "FAIL") {
            //商户自行增加处理流程
            #echo "错误代码：" . $unifiedOrderResult['err_code'] . "<br>";
            #echo "错误代码描述：" . $unifiedOrderResult['err_code_des'] . "<br>";
        } elseif ($unifiedOrderResult["code_url"] != NULL) {
            //从统一支付接口获取到code_url
            $code_url = $unifiedOrderResult["code_url"];
            $data['code_url'] = $code_url;

            //商户自行增加处理流程
            //......
        }
        $this->assign('data', $data);
        return $this->fetch();
    }


    public function order_query()
    {
        $out_trade_no = $this->request->param('out_trade_no', '');
        //退款的订单号
        if (!isset($out_trade_no)) {
            return false;
        } else {
            //使用订单查询接口
            $orderQuery = new OrderQuery();
            //设置必填参数
            $orderQuery->setParameter("out_trade_no", $out_trade_no);//商户订单号
            //非必填参数，商户可根据实际情况选填
            //$orderQuery->setParameter("sub_mch_id","XXXX");//子商户号
            //$orderQuery->setParameter("transaction_id","XXXX");//微信订单号
            //获取订单查询结果
            $orderQueryResult = $orderQuery->getResult();
            //商户根据实际情况设置相应的处理流程,此处仅作举例
            if ($orderQueryResult["return_code"] == "FAIL") {
                echo "通信出错：" . $orderQueryResult['return_msg'] . "<br>";
                die;
            } elseif ($orderQueryResult["result_code"] == "FAIL") {
                echo "错误代码：" . $orderQueryResult['err_code'] . "<br>";
                echo "错误代码描述：" . $orderQueryResult['err_code_des'] . "<br>";
                die;
            } else {
                if ($orderQueryResult["return_code"] == 'SUCCESS' && $orderQueryResult["return_msg"] == 'SUCCESS') {
                    $this->success("", "", ['trade_state' => $orderQueryResult['trade_state']]);
                } else {
                    $this->error("查询失败");
                }
            }
        }
    }


    /**
     * 支付成功，仅供开发测试
     */
    public function notifyx()
    {
        $paytype = $this->request->param('paytype');
        $pay = \addons\epay\library\Service::checkNotify($paytype);
        if (!$pay) {
            echo '签名错误';
            return;
        }
        $data = $pay->verify();
        try {
            $payamount = $paytype == 'alipay' ? $data['total_amount'] : $data['total_fee'] / 100;
            $out_trade_no = $data['out_trade_no'];

            //你可以在此编写订单逻辑
        } catch (Exception $e) {
        }
        echo $pay->success();
    }

    /**
     * 支付返回，仅供开发测试
     */
    public function returnx()
    {
        $paytype = $this->request->param('paytype');
        $out_trade_no = $this->request->param('out_trade_no');
        $pay = \addons\epay\library\Service::checkReturn($paytype);
        if (!$pay) {
            $this->error('签名错误');
        }

        //你可以在这里通过out_trade_no去验证订单状态
        //但是不可以在此编写订单逻辑！！！

        $this->success("请返回网站查看支付结果", addon_url("epay/index/index"));
    }

}
