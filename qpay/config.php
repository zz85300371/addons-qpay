<?php

return [
    [
        //配置唯一标识
        'name'    => 'mch_id',
        //显示的标题
        'title'   => '商户ID',
        //类型
        'type'    => 'string',
        //数据字典
        'content' => [
        ],
        //值
        'value'   => '',
        //验证规则 
        'rule'    => 'required',
        //错误消息
        'msg'     => '',
        //提示消息
        'tip'     => '',
        //成功消息
        'ok'      => '',
        //扩展信息
        'extend'  => ''
    ],
    [
        'name'    => 'key',
        'title'   => '商户支付密钥',
        'type'    => 'string',
        'content' => [
        ],
        'value'   => '',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => ''
    ],
    [
        'name'    => 'cert_client',
        'title'   => '证书',
        'type'    => 'string',
        'content' => [
        ],
        'value'   => '/qpay/certs/apiclient_cert.pem',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => ''
    ],
    [
        'name'    => 'cert_key',
        'title'   => '证书密钥',
        'type'    => 'string',
        'content' => [
        ],
        'value'   => '/qpay/certs/apiclient_key.pem',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => ''
    ],
    [
        'name'    => 'notify_url',
        'title'   => '异步通知URL',
        'type'    => 'string',
        'content' => [
        ],
        'value'   => '/addons/qpay/api/notify',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => ''
    ],
    [
        'name'    => 'curl_timeout',
        'title'   => '超时时间',
        'type'    => 'string',
        'content' => [
        ],
        'value'   => '30',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => ''
    ],
];
